package com.example.SpringHelloWorld.Controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SpringHelloWorldController {
@RequestMapping("/")
public String helloworld()
{
	return "Hello World ";
}
}
